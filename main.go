package main

import (
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"regexp"
	"runtime"
	"sync"
	"time"

	tb "gopkg.in/tucnak/telebot.v2"
)

func sed(arg, text string) string {
	var cmd *exec.Cmd
	if runtime.GOOS == "linux" {
		cmd = exec.Command("sed", "--sandbox", arg)
	} else {
		cmd = exec.Command("gsed", "--sandbox", arg)
	}

	stdin, err := cmd.StdinPipe()

	if err != nil {
		fmt.Print(cmd.CombinedOutput())
	}

	go func() {
		defer stdin.Close()
		io.WriteString(stdin, text)
	}()

	out, _ := cmd.CombinedOutput()
	return string(out)
}

func main() {
	var wait sync.WaitGroup

	bot, err := tb.NewBot(tb.Settings{
		Token:  os.Getenv("SEDBOT_TOKEN"),
		Poller: &tb.LongPoller{Timeout: 1 * time.Second},
	})

	if err != nil {
		log.Fatalln(err)
	}

	bot.Handle("/help", func(m *tb.Message) {
		bot.Reply(m, "man sed")
	})

	r, _ := regexp.Compile("^s/.*")

	bot.Handle(tb.OnText, func(m *tb.Message) {
		if r.MatchString(m.Text) && m.IsReply() {
			log.Printf("[%s] %s", m.Sender.Username, m.Text)
			text := sed(m.Text, m.ReplyTo.Text)

			if text == "" {
				text = "[empty message]"
			}

			go func(*tb.Message, string) {
				wait.Add(1)
				bot.Reply(m.ReplyTo, text)
				wait.Done()
			}(m, text)

			bot.Delete(m)
			wait.Wait()
		}
	})

	bot.Start()
}
